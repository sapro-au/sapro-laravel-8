import AppListing from '../app-components/Listing/AppListing';

Vue.component('viruta-listing', {
    mixins: [AppListing]
});