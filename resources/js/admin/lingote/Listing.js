import AppListing from '../app-components/Listing/AppListing';

Vue.component('lingote-listing', {
    mixins: [AppListing]
});