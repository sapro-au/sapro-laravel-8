import AppListing from '../app-components/Listing/AppListing';

Vue.component('metale-listing', {
    mixins: [AppListing]
});