<?php
namespace Modules\Bloque\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BarraBloqueFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Bloque\Entities\BarraBloque::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}

