<?php
namespace Modules\Barra\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BarraEstadoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Barra\Entities\BarraEstado::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}

