<?php
namespace Modules\Inconformidad\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class InconformidadFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Inconformidad\Entities\Inconformidad::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}

