<?php
namespace Modules\Salidas\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ColectoresFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Salidas\Entities\Colectores::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}

