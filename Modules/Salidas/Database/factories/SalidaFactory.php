<?php
namespace Modules\Salidas\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class SalidaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Salidas\Entities\Salida::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}

