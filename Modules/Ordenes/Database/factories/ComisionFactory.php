<?php
namespace Modules\Ordenes\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ComisionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Ordenes\Entities\Comision::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}

