<?php
namespace Modules\Ordenes\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class OrdenRegistroFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Ordenes\Entities\OrdenRegistro::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}

