<?php
namespace Modules\Lingotes\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BarraLingoteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Lingotes\Entities\BarraLingote::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}

