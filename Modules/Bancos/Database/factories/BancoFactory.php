<?php
namespace Modules\Bancos\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BancoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Bancos\Entities\Banco::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}

