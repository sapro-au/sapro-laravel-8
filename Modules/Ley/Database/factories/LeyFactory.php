<?php
namespace Modules\Ley\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class LeyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Ley\Entities\Ley::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}

