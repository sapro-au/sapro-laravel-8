<?php
namespace Modules\Viruta\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class VirutaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Viruta\Entities\Viruta::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}

